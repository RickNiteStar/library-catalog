import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AccountSync {
    private static Account account = new Account();

    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();

        for(int i=0;i<100;i++) {
            executor.execute(new AddAPennyTask());
        }

        executor.shutdown();
        while(!executor.isTerminated()) {
        }
    }

    private static class AddAPennyTask implements Runnable{
        public void run() {
            account.deposit(1);
        }
    }

    private static class Account{
        private int balance = 0;

        public void deposit(int amount) {
            synchronized(Account.class) {
                int newBalance = balance + amount;

                balance = newBalance;

            }
        }
    }
}