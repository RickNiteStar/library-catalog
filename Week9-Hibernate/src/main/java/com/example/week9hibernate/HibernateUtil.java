package com.example.week9hibernate;

import com.fasterxml.classmate.AnnotationConfiguration;
import com.fasterxml.classmate.AnnotationInclusion;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;
import java.lang.annotation.Annotation;

class HibernateUtil
{
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory()
    {
        try
        {
            // Create the SessionFactory from hibernate.cfg.xml
            return new AnnotationConfiguration() {
                public HibernateUtil configure(File file) {
                    return null;
                }

                @Override
                public AnnotationInclusion getInclusionForClass(Class<? extends Annotation> aClass) {
                    return null;
                }

                @Override
                public AnnotationInclusion getInclusionForConstructor(Class<? extends Annotation> aClass) {
                    return null;
                }

                @Override
                public AnnotationInclusion getInclusionForField(Class<? extends Annotation> aClass) {
                    return null;
                }

                @Override
                public AnnotationInclusion getInclusionForMethod(Class<? extends Annotation> aClass) {
                    return null;
                }

                @Override
                public AnnotationInclusion getInclusionForParameter(Class<? extends Annotation> aClass) {
                    return null;
                }
            }.configure(new File("hibernate.cgf.xml")).buildSessionFactory();
        }
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }
}