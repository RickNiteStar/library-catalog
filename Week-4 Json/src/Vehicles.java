public class Vehicles {

    private String type;
    private String make;
    private String model;
    private String year;
    private String color;
    private Integer miles;

    public String getType() {
        return type;
    }
    public String getMake() {
        return make;
    }
    public String getModel() {
        return model;
    }
    public String getYear() {
        return year;
    }
    public  String getColor() {
        return color;
    }

    public Integer getMiles() {
        return miles;
    }
    public void setType(String type) {
        this.type=type;
    }

public void setMake(String make) {
        this.make=make;
}

    public void setColor(String color) {
        this.color = color;
    }

    public void setMiles(Integer miles) {
        this.miles = miles;

            }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYear(String year) {
        this.year = year;
    }

public String toString() {
        return "Vehicle Make " + make + "Vehicle Type "
                + make + "Vehicle Model " + model + "Vehicle Color " + color + "What model year "
                + year + "Odometer reading " + miles + ".";

}
}


