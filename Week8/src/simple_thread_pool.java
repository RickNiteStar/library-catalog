package com.journaldev.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class simple_thread_pool {

    public static void main(String[] args) {
        ExecutorService new_executor = Executors.newFixedThreadPool(7);     // fixed size thread-pool of 7 work_thread
        for (int i = 0; i < 12; i++) {                                     // loop till 12 jobs, so remaining 5 will wait in the queue
            Runnable worker = new com.journaldev.threadpool.thread_work("" + i);                    // created an instence of thread_work class
            new_executor.execute(worker);                                // a thread is in a new state
        }
        new_executor.shutdown();
        while (!new_executor.isTerminated()) {                        // terminated that thread so can't be resumed
        }
        System.out.println("\nFinished all threads ! ");            // print
    }
}