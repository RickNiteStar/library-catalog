import java.util.regex.Pattern;

public class Employee {
    // instance variables
    private String name;
    private String employeeNumber;
    private String hireDate;

    /**
     * Parameterized constructor
     *
     * @param n
     * @param num
     * @param date
     */
    public Employee(String n, String num, String date) {
        this.employeeNumber = num;
        this.name = n;
        this.hireDate = date;
    }

    /**
     * Default constructor
     */
    public Employee() {
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param n the name to set
     */
    public void setName(String n) {
        this.name = n;
    }

    /**
     * @return the employeeNumber
     */
    public String getEmployeeNumber() {
        return employeeNumber;
    }

    /**
     * @param employeeNumber the employeeNumber to set
     */
    public void setEmployeeNumber(String e) {
        if (isValidEmpNum(e)) {
            this.employeeNumber = e;
        }

    }

    /**
     * @return the hireDate
     */
    public String getHireDate() {
        return hireDate;
    }

    /**
     * @param hireDate the hireDate to set
     */
    public void setHireDate(String hireDate) {
        this.hireDate = hireDate;
    }

    public boolean isValidEmpNum(String e) {
        String pattern = "[0-9]{3}[-]{1}[A-M]{1}";
        return Pattern.matches(pattern, e);
    }

    @Override
    public String toString() {
        return "Name: " + getName() + "\n" + "Employee Number: " + getEmployeeNumber() + "\n" + "Hire Date: "
                + getHireDate() + "\n";
    }

}