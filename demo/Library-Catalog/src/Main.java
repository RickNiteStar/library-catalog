public class Main {

    public static void main(String[] args) {
        // Create an object of Catalog
        Catalog catalog = new Catalog();
        // add books to catalog
        catalog.add(new Book("Nightmare", "Piers", "Anthony"));
        catalog.add(new Book("The Hitchhikers Guide to the Galaxy", "Douglas", "Adams"));
        catalog.add(new Book("I Robot", "Isaac", "Asimov"));
        catalog.add(new Book("The Shining", "Stephen", "King"));
        // Call methods
        System.out.println(catalog.getBook("1"));
        System.out.println(catalog.getBook("2"));
        System.out.println(catalog.getBook("3"));
        System.out.println(catalog.getBook("4"));
        System.out.println(catalog.checkout("1"));
        System.out.println(catalog.getBook("1"));
        System.out.println(catalog.checkin("1"));
        System.out.println(catalog.getBook("1"));
        System.out.println(catalog.remove("1"));
        System.out.println(catalog.remove("1"));

    }

}

