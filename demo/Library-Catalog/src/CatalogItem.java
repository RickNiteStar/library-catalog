public class CatalogItem {
    private Book book;
    private String id;
    boolean available;

    /**
     * @param book
     * @param id
     * @param availability
     */
    public CatalogItem(Book book, String id, boolean availability) {
        this.book = book;
        this.id = id;
        this.available = availability;
    }

    /**
     * @return the book
     */
    public Book getBook() {
        return book;
    }

    /**
     * @param book the book to set
     */
    public void setBook(Book book) {
        this.book = book;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the availability
     */
    public boolean isAvailable() {
        return available;
    }

    public void setAvailability() {
        this.available = true;
    }

    public void setUnAvailability() {
        this.available = false;
    }

}