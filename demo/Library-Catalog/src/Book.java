public class Book {
    // instance variables
    private String title;
    private String authorFirstName;
    private String authorLastName;

    /**
     * @param title
     * @param firstName
     * @param lastName
     */
    public Book(String title, String firstName, String lastName) {
        this.title = title;
        this.authorFirstName = firstName;
        this.authorLastName = lastName;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the authorFirstName
     */
    public String getAuthorFirstName() {
        return authorFirstName;
    }

    /**
     * @param authorFirstName the authorFirstName to set
     */
    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    /**
     * @return the authorLastName
     */
    public String getAuthorLastName() {
        return authorLastName;
    }

    /**
     * @param authorLastName the authorLastName to set
     */
    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

    @Override
    public String toString() {
        return "Book [Title=" + title + ", Author First Name=" + authorFirstName + ", Author Last Name="
                + authorLastName + "]";
    }

}

