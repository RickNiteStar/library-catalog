import java.util.ArrayList;

public class Catalog {
    private ArrayList<CatalogItem> catalogItems = null;
    static int id = 0;

    /**
     * No-argument constructor
     */
    public Catalog() {
        catalogItems = new ArrayList<>();
    }

    // add book to catalog
    public String add(Book book) {
        // Adds a book to the catalog collection, ensures the newly added book is
        // available for checkout.
        Catalog.id++;
        CatalogItem catalogItem = new CatalogItem(book, String.valueOf(Catalog.id), true);
        catalogItems.add(catalogItem);
        return "Book Added successfully";
    }

    // If the book with the specified id is in the catalog's book collection and is
    // available for checkout,
    // the Catalog class marks this book as unavailable and returns true
    public boolean checkout(String id) {
        for (CatalogItem catalogItem : catalogItems) {
            if (catalogItem.getId().equals(id) && catalogItem.isAvailable()) {
                catalogItem.setUnAvailability();
                return true;
            }
        }
        return false;
    }

    // If the book with the specified id is in the catalog's book collection and is
    // currently checked out,
    // the Catalog marks this book as available and returns true
    public boolean checkin(String id) {
        for (CatalogItem catalogItem : catalogItems) {
            if (catalogItem.getId().equals(id) && !catalogItem.isAvailable()) {
                catalogItem.setAvailability();
                return true;
            }
        }
        return false;
    }

    // Searches the book catalog for books that match the searchTerm exactly
    // (equals) in the Title, First Name, or Last name attributes of the Book class.
    // Returns the ArrayList of String objects, corresponding to unique IDs that
    // matched the searchTerm.
    public ArrayList<String> search(String searchTerm) {
        ArrayList<String> ids = new ArrayList<>();
        for (CatalogItem catalogItem : catalogItems) {
            if (catalogItem.getBook().getTitle().equals(searchTerm)
                    || catalogItem.getBook().getAuthorFirstName().equals(searchTerm)
                    || catalogItem.getBook().getAuthorLastName().equals(searchTerm)) {
                ids.add(catalogItem.getId());
            }
        }
        return ids;
    }

    // Returns the Book object that corresponds to the unique id passed as an
    // argument.
    // If a book with the given id does not exist, returns null.
    public Book getBook(String id) {
        for (CatalogItem catalogItem : catalogItems) {
            if (catalogItem.getId().equals(id)) {
                return catalogItem.getBook();
            }
        }
        return null;
    }

    // If the book with the specified id is in the Catalog's book collection and is
    // available for checkout,
    // removes the book from the collection and returns true. Otherwise, returns
    // false.
    public boolean remove(String id) {
        for (CatalogItem catalogItem : catalogItems) {
            if (catalogItem.getId().equals(id) && catalogItem.isAvailable()) {
                catalogItems.remove(catalogItem);
                return true;
            }
        }
        return false;
    }
}