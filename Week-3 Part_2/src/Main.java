import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n1, n2;
        while (true) {
            System.out.println("Enter two whole numbers");
            n1 = sc.nextInt();
            n2 = sc.nextInt();
            if (n2 != 0)
                break;
            else
                System.out.println("Invalid..your second number has to be greater than zero. Try again");
        }
        int s = div(n1, n2);
        int s1 = div(n2, n1);
        System.out.println("The result of " + n1 +" divided by " + n2 + " is: " + s);
        System.out.println("Or you could reverse it & say the result of " + n2 +" divided by " + n1 + " is: " + s1);
    }

    public static int div(int n1, int n2) throws ArithmeticException {
        return n1 / n2;
    }
}